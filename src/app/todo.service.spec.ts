import { LoggerService } from "./logger.service";
import { TodoService } from "./todo.service";
import { TODOS } from './test-data/todo.db';
import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('TodoService', () => {

    let todoService: TodoService;
    let loggerSpy: any;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        loggerSpy = jasmine.createSpyObj('LoggerService', ['log']);
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                TodoService,
                { provide: LoggerService, useValue: loggerSpy }
            ]
        });
        todoService = TestBed.inject(TodoService)
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    it('debería agregar una nueva tarea', () => {
        // const logger = new LoggerService;
        // spyOn(logger, 'log');
        todoService.add({autor: 'PruebaAutor', titulo: 'PruebaTitulo', descripcion: 'PruebaDescripcion'});
        
        expect(todoService.todos.length).toBe(1, 'Deberiamos tener una unica tarea');
        expect(todoService.todos[0].id).toBe(1, 'El id autoincremental debría ser 1');
        expect(todoService.autoIncrementId).toBe(2, 'El autoincremental debería haber avanzado');
        expect(todoService.todos[0].titulo).toEqual('PruebaTitulo', 'El titulo debería coincidir con la prueba');

        expect(loggerSpy.log).toHaveBeenCalledTimes(1);
    });

    it('debería borrar una tarea', () => {
        const todoBorrar = [...TODOS];
        todoService.todos = todoBorrar;

        todoService.delete(2);

        expect(todoService.todos.length).toBe(2, 'El número de tareas debería ser 2');
        expect(todoService.todos[1].autor).toEqual('Sara')
    })

    it('debería recuperar todas las tareas', () => {
        todoService.getAll().subscribe(todos => {
            expect(todos).toBeTruthy('No existen las tareas');
            expect(todos.length).toBe(3, 'La longitud debería ser de 3 tareas');

            const todo = todos.find(item => item.id === 2);
            expect(todo.titulo).toEqual('Compra de mueble', 'El título debe ser el especificado en las pruebas')
        });
        const req = httpTestingController.expectOne('http://localhost:3000/api/todos/all');
        expect(req.request.method).toBe('GET');
        req.flush(TODOS)
    })

    it('debería recuperar una única tarea', () => {
        todoService.getById(2).subscribe(todo => {
            expect(todo).toBeTruthy('La tarea debe existir')
            expect(todo.id).toBe(2, 'El id de la tarea debe ser 2')
        });

        const req = httpTestingController.expectOne('http://localhost:3000/api/todos/2');
        expect(req.request.method).toBe('GET')
        req.flush(TODOS[1]);
    });

    afterEach(() => {
        httpTestingController.verify();
    });
});