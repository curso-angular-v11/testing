import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { DebugElement } from '@angular/core';
import { AppModule } from '../app.module';
import { By } from '@angular/platform-browser';

fdescribe('HomeComponent', () => {

    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;
    let el: DebugElement;

    beforeEach(waitForAsync(() =>{
        TestBed.configureTestingModule({
            imports: [AppModule]
        }).compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(HomeComponent);
                component = fixture.componentInstance;
                el = fixture.debugElement; 
            });
    }));

    it('debería existir el componente', () => {
        expect(component).toBeTruthy();
    });

    fit('debería agregaruna tarea', fakeAsync(() => {
        setInputValue('.form-control.autor', 'pruebaAutor2');
        setInputValue('.form-control.titulo', 'pruebaTitulo');
        setInputValue('.form-control.descripcion', 'pruebaDescripcion');

        const boton = el.query(By.css('.btn.btn-success'));
        boton.nativeElement.click();
        tick();
        fixture.detectChanges();

        const card = el.query(By.css('.card:first-child'));
        const title = el.query(By.css('.card-title'));
        const descripcion = el.query(By.css('.card-text'));

        expect(card).toBeTruthy();
        expect(title.nativeElement.textContent).toBe('pruebaTitulo');
        expect(descripcion.nativeElement.textContent).toBe('pruebaDescripcion');
    }));

    function setInputValue(selector: string, value: string) {
        fixture.detectChanges();
        tick();

        const inputAutor = el.query(By.css(selector));
        inputAutor.nativeElement.value = value;
        // ahora forzamos que el evento input se ejecute sobre el campo de texto
        inputAutor.nativeElement.dispatchEvent(new Event('input'));
        // para asegurarnos de q se ejecute el evento input antes de ka propia prueba usaremos thick para darle tiempo a la prueba
        tick();
    }
})